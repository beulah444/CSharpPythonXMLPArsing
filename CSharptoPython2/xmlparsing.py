import sys
import xml.parsers.expat
import xml.etree.ElementTree as ET
tree = ET.parse('test.xml')
root = tree.getroot()
def printing():
	print(root) #root node
	print(root.tag) #root tag
	#root has dictionary of attributes
	print(root.attrib)
	print(root[0].attrib)
	print(root[1].attrib)
	print(root[2].attrib)
	#Accessing the nested children , such as access the specific child nodes by index:
	for child in root:
		print(child.tag,child.attrib)
		#Prints the text inside the child attributes
		print(root[0][0].text)
		print(root[0][1].text)
		print(root[0][2].text)
		print(root[1][0].text)
		print(root[1][1].text)
		print(root[1][2].text)
		print(root[2][0].text)
		print(root[2][1].text)
		print(root[2][2].text)
#Finding Elements
#iterate recursively over all the sub-tree below it (its children, their children, and so on) using Element.iter()
for neighbor in root.iter('neighbor'):
	print(neighbor.attrib)
	#Element.findall() finds only elements with a tag which are direct children of the current element
	for country in root.findall('country'):
		#Element.find() finds the first child with a particular tag, and Element.text accesses the elements text content.
		rank = country.find('rank').text
		#Element.get() accesses the elements attributes
		name = country.get('name')
		print(name, rank)




