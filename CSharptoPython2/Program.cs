﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IronPython.Hosting; // make us use Python
using Microsoft.Scripting.Hosting;

//public class dynamic_demo
//{
//    static void Main()
//    {
//        var ipy = Python.CreateRuntime();
//        dynamic test = ipy.UseFile(@"C:\Users\beula\Desktop\Test1.py");
//        test.Simple();
//        Console.ReadKey();
//    }
//}

public class dynamic_demo
{
    static void Main()
    {
       
        var engine = Python.CreateEngine();
        var pyScope = engine.CreateScope();
        var paths = engine.GetSearchPaths();
        paths.Add(@"C:\Program Files (x86)\IronPython 2.7\Lib");
        engine.SetSearchPaths(paths);
        // string script = @"C:\Program Files (x86)\IronPython 2.7\Lib";
        //engine.ExecuteFile(script, pyScope);
        //engine.ExecuteFile(@"D:\parse\parse.py");
        var ipy = engine.Runtime;//Python.CreateRuntime();
        dynamic test = ipy.UseFile(@"D:\parse\test.py");
       // ipy.ExecuteFile(@"D:\parse\parse.py");
        test.Xmlparsing();
        Console.ReadKey();
    }
}

//namespace ConsoleApplication2
//{
//    class Program
//    {
//        static void Main(string[] args)
//        {

//            int a = 1;
//            int b = 2;

//            Microsoft.Scripting.Hosting.ScriptEngine py = Python.CreateEngine(); // allow us to run ironpython programs
//            Microsoft.Scripting.Hosting.ScriptScope s = py.CreateScope(); // you need this to get the variables
//            py.Execute("x = " + a + "+" + b, s); // this is your python program
//            Console.WriteLine(s.GetVariable("x")); // get the variable from the python program
//            Console.ReadLine(); // wait for the user to press a button
//        }
//    }
//}