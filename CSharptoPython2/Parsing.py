import sys
import xml.etree.ElementTree as etree
def Xmlparsing():
    tree = etree.parse('Personal.xml')
    #root node
    root = tree.getroot()
    print(root)
    #root has a tag and a dictionary of attributes:
    print(root.tag)
    print(root.attrib)
    print(root[0].attrib)
    print(root[1].attrib)
    print(root[2].attrib)
    print(root[3].attrib)
    #Accessing the nested children , such as access the specific child nodes by index:
    for child in root:
        print(child.tag,child.attrib)
        print(root[0][0].text)
        print(root[0][1].text)
        print(root[1][0].text)
        print(root[1][1].text)
        print(root[2][0].text)
        print(root[2][1].text)
        print(root[3][0].text)
        print(root[3][1].text)
    #Finding Elements
    #iterate recursively over all the sub-tree below it (its children, their children, and so on) using Element.iter()
    for neighbor in root.iter('neighbor'):
        print(neighbor.attrib)
    #Element.findall() finds only elements with a tag which are direct children of the current element
    for person in root.findall('Person'):
        rank = person.find('rank').text#Element.find() finds the first child with a particular tag, and Element.text accesses the element’s text content.
        name = person.get('name')#Element.get() accesses the element’s attributes
        print(name, rank)
