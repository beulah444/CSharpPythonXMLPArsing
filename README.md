Steps to install and run this code
1. Download and Install the below packages using NugetManager in Microsoft .Net Visual Studio
DynamicLanguageRuntime,IronPython, IronPython.Interpreter,IronPython.StdLib,PythonLibs4CSharp
2. I also installed IronPython lastest version 2.7 from below link
http://ironpython.net/
3.IronPython package doesnot contain few modules,which we need to install manually
i)expat.py 
copy this script from source
https://svn.code.sf.net/p/fepy/code/trunk/lib/pyexpat.py
save it in your local machine
C:\Program Files (x86)\IronPython 2.7\Lib\xml\parsers
ii)ElementTree Module
Downloaded it from below link, and installed it by copying the source to ..\IronPython 2.7\Lib\site-packages\.
http://effbot.org/downloads#elementtree

sources I found from that worked for me:
https://stackoverflow.com/questions/19576235/ironpython-and-xml-etree-elementtree-illegal-characters-error

http://grokbase.com/t/python/ironpython-users/1146hab9aa/ironpython-ironpython-and-xml-parsing

http://ironpython.net/

